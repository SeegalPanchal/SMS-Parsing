/* bow.c
  Main Functions file
  By: Seegal Panchal (panchals@uoguelph.ca)
  Student ID: 1016249
*/

#include "bow.h"

/* this function represents a single word and
the number of times that it occurs in memory */
struct bag_struct *new_bag() {
  struct bag_struct *new_bag_struct;
  /* allocate enough memory for the structure */
  new_bag_struct = malloc(sizeof(struct bag_struct));

  new_bag_struct->bag = NULL;
  new_bag_struct->bag_size = 0;
  new_bag_struct->total_words = 0;

  return new_bag_struct;
}

/* print the bag */
void print_bag(struct bag_struct *bow) {
  int i;

  for(i = 0; i < bow->bag_size; i++) {
    printf("%d: ", i);
    print_word_count(bow->bag+i);
  }

  printf("Total Words: %d\n", bow->total_words);
}

/* free the entire word count structure, including variable
using only 1 call rather than multiple free() */
void free_bag(struct bag_struct *bow) {
  int i;
  for (i = 0; i < bow->bag_size; i++) {
    free(bow->bag[i].word);
  }
  free(bow->bag);
  free(bow);
  return;
}

/* This function will perform a binary search
of the "bag" array contained in the struct
bag_struct pointed to by the pointer bow. */
int bagsearch(struct bag_struct *bow, char *word) {
  return binary_search(bow, 0, bow->bag_size-1, word);
}

/* binary search using integers rather than memory */
int binary_search(struct bag_struct *bow, int left, int right, char *word) {
  /*
  right-left -> returns the size of the section we are looking at
  (right-left)/2 -> gives us the middle of the section
  (right-left)/2+left -> put that middle assuming left is 0
  */
  int middle = ((right-left)/2)+left;
  int compare;

  if (middle < 0) {
    return 0;
  }

  if (left > right) {
    return middle;
  }

  compare = strcmp(bow->bag[middle].word, word);
  if (compare > 0) {
    return binary_search(bow, left, middle-1, word);
  } else if (compare < 0) {
    return binary_search(bow, middle+1, bow->bag_size-1, word);
  } else {
    return middle;
  }
}

/* This function should loop over the characters
in the string that is pointed to by string_ptr.*/
char *get_word(char **string_ptr) {

  char *word;
  /* count the index of string_ptr */
  int count = 0;
  /* allocate 919 bytes to memory as it is
  the longest word in the SMSSpamCollection */
  word = malloc(919);
  /* check if there is a string at all */
  while (!is_letter(**string_ptr) && **string_ptr != '\0') {
    *string_ptr += 1;
  }

  while (is_letter(**string_ptr)) {
    word[count] = **string_ptr;
    *string_ptr += 1;
    count++;
  }

  if (**string_ptr == '\0') {
    free(word);
    return NULL;
  }
  /* reallocate just enough memory for word and a terminating char */
  word = realloc(word, sizeof(char)*strlen(word)+1);
  word[count] = '\0';
  /* *string_ptr += strlen(*string_ptr); */
  return word;
}

/* check if the character is a letter or not */
int is_letter(char character) {

  /*
  A = 65 | a = 97
  Z = 90 | z = 122
  */
  /* if withing the integer values of
  the upper and lowercase alphabet */
  if ((character >= 'a' && character <= 'z')
  || (character >= 'A' && character <= 'Z')) {
    /* return 1 if the character is a letter */
    return 1;
  }

  /* return 0 if the character is not a letter */
  return 0;
}

/* This function should allocate a new struct
word_count_struct and return a pointer to it.
Additionally, it should set the "word"
pointer to point to the argument. */
struct word_count_struct *new_word_count(char *word) {
  struct word_count_struct *word_count;
  int i;
  /* allocate enough memory for the structure */
  word_count = malloc(sizeof(struct word_count_struct));

  /* if there is a word, convert it to lower case */
  if (word != NULL) {
    for (i = 0; i < strlen(word); i++) {
      if (word[i] >= 'A' && word[i] <= 'Z') {
        word[i] = word[i] + 32;
      }
    }
  }

  word_count->word = word;
  word_count->count = 1;

  return word_count;
}

/* print the word count struct */
void print_word_count(struct word_count_struct *word_count) {

  /* print out to see what's happening */
  if (word_count->word != NULL) {
    printf("%s = %d\n", word_count->word, word_count->count);
    return;
  }
  return;
}

/* free the entire word count structure, including variable
using only 1 call rather than multiple free() */
void free_word_count(struct word_count_struct *word_count) {
  free(word_count->word);
  free(word_count);
  return;
}

/* This function should search the function using
bagsearch and alphabetically organize the text file */
void add_word(struct bag_struct *bow, char *word) {
  struct word_count_struct *word_count;
  int pos;

  word_count = new_word_count(word);

  /* reallocate enough memory for another word count struct */
  bow->bag = realloc(bow->bag, (bow->bag_size+1)*sizeof(struct word_count_struct));


  pos = bagsearch(bow, word);


  /* as long as we are looking for a word,
  the total number of words will go up */
  bow->total_words++;

  /* if the word already exists */
  printf("Pos: %d, BagSize: %d\n", pos, bow->bag_size);
  if (bow->bag[pos].word != NULL && strcmp(bow->bag[pos].word, word) == 0) {
    bow->bag[pos].count++;
  } else {

    bow->bag_size++;
    /* take the structures from teh current position,
    all the way to the end of the array, and copy them 1 spot
    to the right */

    memcpy(bow->bag+pos+1, bow->bag+pos, ((bow->bag_size-1)-pos)*sizeof(struct word_count_struct));

    /* replace word in the spot we are looking at with
    the word we want */

    memcpy(bow->bag+pos, word_count, sizeof(struct word_count_struct));

  }

  free(word_count);
  return;
}

/* This function should identify all the words
in the string line (using the get word function),
and add them to the bow.*/
void add_text(struct bag_struct *bow, char *line) {

  char *word;
  while (*line)  /* while sentence doesn't point to the '\0' character at the end of the string */
  {
    word = get_word(&line);  /* this will allocate memory for a word */
    if (word != NULL) {
      add_word(bow, word);
    }
  }
  return;
}

/* This function should open the file:
SMSSpamCollection and read it one line at a time. */
void read_sms_data(struct bag_struct *bow, char *label) {

  FILE *fp;
  fp = fopen("SMSSpamCollection", "r");
  if (fp != NULL) {
    while (!feof(fp)) {
      char *read, *temp, *sentence;
      read = malloc(924);
      fgets(read, 924, fp);
      read = realloc(read, sizeof(char) * (strlen(read)+1));
      read[strlen(read)] = '\0';
      temp = strtok(read, "\t");
      if (temp != NULL && strcmp(temp, label)==0) {
        sentence = strtok(NULL, "\t");
        add_text(bow, sentence);
      }
    }
  } else {
    printf("Error accessing file.\n");
  }
  fclose(fp);
  return;
}

/* This function should go through the entries of ham and the spam.
It will need to go through them asynchronously --- that is, both
files at the same time, but not necessarily at the same speed.
At each step through the loop it should look at the "current" word
from each bag_struct.  If the two words are the same then the program
should (floating point) divide their counts, by their respective
total_words computing two word frequencies (how often that word occurs
divided by the total number of words that are in the bag).
If one word comes alphabetically before the other, then the alphabetically
first word's count should be divided by its respective total_words
to compute the frequency, while the second frequency should be calculated
as 1 divided by the other bag's total words (this is called a pseudo count).
If neither of the two frequencies is above 0.005 (i.e. 0.5%),
then we discard the word as too rare to be useful.  Next we will calculate
the ratio of the two frequencies (divide one by the other) to calculate
how much more often one word occurs compared to the other in the
"ham" vs "spam" bags.  If the ratio is less than 1, we will invert it
(divide 1 by it). Finally, if the ratio is greater than 2.0, use the
following formatting string "%20s %8.6f %8.6f %10.6f %s" to print the word,
the ham frequency, the spam frequency, the final ratio and either the word
"ham" or "spam" if the word occurs more frequently in in "ham" or "spam"
respectively.  After processing one word, move on to the next work.
NOTE:  advance your position in both bags if the two words matched; otherwise,
only advance your position in the bag that had the alphabetically earlier word.*/
void differences(struct bag_struct *ham, struct bag_struct *spam) {
  int ham_index = 0;
  int spam_index = 0;
  float ratio = 0;
  char *greater_frequency;
  float ham_frequency = 0;
  float spam_frequency = 0;

  /* go through them asynchronously */

  while (ham_index < ham->bag_size-1 || spam_index < spam->bag_size-1) {

    int compare = strcmp(ham->bag[ham_index].word, spam->bag[spam_index].word);
    char *word;

    /*If one word comes alphabetically before the other, then the alphabetically
    first word's count should be divided by its respective total_words
    to compute the frequency, while the second frequency should be calculated
    as 1 divided by the other bag's total words (this is called a pseudo count).*/

    /* SPAM word comes alphabetically before HAM */

    if (compare > 0) {
      spam_frequency = (float)spam->bag[spam_index].count / (float)spam->total_words;
      ham_frequency = (float)1 / (float)ham->total_words;
      word = spam->bag[spam_index].word;
      spam_index++;
    }
    /* HAM word comes alphabetically before SPAM */
    else if (compare < 0) {
      ham_frequency = (float)ham->bag[ham_index].count / (float)ham->total_words;
      spam_frequency = (float)1 / (float)spam->total_words;
      word = ham->bag[ham_index].word;
      ham_index++;
    }
    /* If the two words are the same then the program
    should (floating point) divide their counts, by their respective
    total_words computing two word frequencies (how often that word occurs
    divided by the total number of words that are in the bag). */
    else {
      ham_frequency = (float)ham->bag[ham_index].count / (float)ham->total_words;
      spam_frequency = (float)spam->bag[spam_index].count / (float)spam->total_words;
      /* either work here anyways, so I just used ham */
      word = ham->bag[ham_index].word;
      ham_index++;
      spam_index++;
    }

    /* If neither of the two frequencies is above 0.005 (i.e. 0.5%),
    then we discard the word as too rare to be useful. */
    if (ham_frequency < 0.005 && spam_frequency < 0.005) {

      ham_frequency = 0;
      spam_frequency = 0;

    }

    /*Next we will calculate
    the ratio of the two frequencies (divide one by the other) to calculate
    how much more often one word occurs compared to the other in the
    "ham" vs "spam" bags.  If the ratio is less than 1, we will invert it
    (divide 1 by it).*/
    else {

      ratio = ham_frequency / spam_frequency;
      if (ratio < 1) {
        ratio = 1 / ratio;
        greater_frequency = "spam";
      } else {
        greater_frequency = "ham";
      }
      /* if the ratio is greater than 2.0, use the
      following formatting string "%20s %8.6f %8.6f %10.6f %s" to print the word,
      the ham frequency, the spam frequency, the final ratio and either the word
      "ham" or "spam" if the word occurs more frequently in in "ham" or "spam"
      respectively. */
      if (ratio > 2) {
        printf("%20s %8.6f %8.6f %10.6f %s\n", word, ham_frequency, spam_frequency, ratio, greater_frequency);
      }
    }

  }
  return;
}

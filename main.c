/* main.c
  main file
  By: Seegal Panchal (panchals@uoguelph.ca)
  Student ID: 1016249
*/

#include "bow.h"

int main(int argc, char **argv) {
/*
  char *sentence = "#The quick brown fox jumped over 23&%^24 the lazy dogs.";

  char *sentence = "t h e q u i c k b r o w n f o x j u m p e d o v e r t h e l a z y d o g s";
*/
  struct bag_struct *ham;
  struct bag_struct *spam;
  char *label;

  ham = new_bag();
  label = "ham";
  read_sms_data(ham, label);

  spam = new_bag();
  label = "spam";
  read_sms_data(spam, label);

  differences(ham, spam);

  free_bag(ham);
  free_bag(spam);
  return 0;
}

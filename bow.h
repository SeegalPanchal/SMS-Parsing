/* bow.h
  Header File
  By: Seegal Panchal (panchals@uoguelph.ca)
  Student ID: 1016249
*/

#ifndef BOW_H
#define BOW_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/* this function represents a single word and
the number of times that it occurs in memory */
struct word_count_struct {
  char *word;
  int count;
};

/* this function represents all the word and count
pairs as well as the total number of such pairs */
struct bag_struct {
  struct word_count_struct *bag;
  int bag_size;
  int total_words;
};

/* This function will return a pointer to a newly
allocated bag_struct with a bag_size of zero,
and total_words of zero. */
struct bag_struct *new_bag();
/* print the bag */
void print_bag(struct bag_struct *bow);
/* free all the variables in the bag */
void free_bag(struct bag_struct *bow);

/* This function will perform a binary search
of the "bag" array contained in the struct
bag_struct pointed to by the pointer bow. */
int bagsearch(struct bag_struct *bow, char *word);
int binary_search(struct bag_struct *bow, int left, int right, char *word);

/* This function should loop over the characters
in the string that is pointed to by string_ptr.*/
char *get_word(char **string_ptr);
/* check if the character is a lette or not */
int is_letter(char character);

/* This function should allocate a new struct
word_count_struct and return a pointer to it.
Additionally, it should set the "word"
pointer to point to the argument. */
struct word_count_struct *new_word_count(char *word);
/* print the word count struct */
void print_word_count(struct word_count_struct *word_count);
/* free the structure as well as the variables inside the struc */
void free_word_count(struct word_count_struct *word_count);

/* This function should search the function using
bagsearch and alphabetically organize the text file */
void add_word(struct bag_struct *bow, char *word);

/* This function should identify all the words
in the string line (using the get word function),
and add them to the bow.*/
void add_text(struct bag_struct *bow, char *line);

/* This function should open the file:
SMSSpamCollection and read it one line at a time. */
void read_sms_data(struct bag_struct *bow, char *label);

/* This function should go through the entries of ham and the spam. */
void differences( struct bag_struct *ham, struct bag_struct *spam );

#endif

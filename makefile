bag: bow.o main.o
		gcc -Wall -ansi -pedantic bow.o main.o -o bag

bow.o: bow.c bow.h
		gcc -Wall -ansi -pedantic -c bow.c -o bow.o

main.o: main.c bow.h
		gcc -Wall -ansi -pedantic -c main.c -o main.o

clean:
		rm -f bag bow.o main.o
		clear
